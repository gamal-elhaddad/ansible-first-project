My First Ansible Project, Automate NodeJs App Deployment.

###### first you need to clone the NodeJs app to your local machine: 
    
    git clone https://gitlab.com/gamal-elhaddad/nodejs-application.git

###### to colne this Ansible ptoject :

    git clone https://gitlab.com/gamal-elhaddad/ansible-first-project.git

###### to start and execute tasks in ansible playbook:

    ansible-playbook -i hosts-inv deploy-nodejs.yaml

**Don't forget to change the ip of host in hosts-inv to your remote machine ip.            **
